(function() {
  var bodyparser, cookieparser, dbs, express, fs, harp, ld, mkdirp, mountConnect, mountDatabase, mountEntityParser, mountKeyValParser, mountLoginPage, mountMiddleware, mountRoutes, mountSchemaLoader, mountStatic, mountToolkit, mountValidator, path, rp, setupapp, tk, tv, ut, validators;

  path = require('path');

  fs = require('fs');

  harp = require('harp');

  express = require('express');

  bodyparser = require('body-parser');

  cookieparser = require('cookie-parser');

  mkdirp = require('mkdirp');

  tv = require('tv4');

  ld = require('lodash');

  rp = require('./lib/routeparser');

  ut = require('./lib/util');

  tk = require('./lib/toolkit');

  dbs = require('./lib/databases');

  validators = require('./lib/validators');

  setupapp = function(options) {
    var app;
    app = options.app || express();
    if (options.app == null) {
      options.app = app;
    }
    return app;
  };

  mountStatic = function(options) {
    var app;
    options.jsdir = path.join(process.cwd(), options.jsdir || 'static/js');
    options.cssdir = path.join(process.cwd(), options.cssdir || 'static/css');
    options.viewsdir = path.join(process.cwd(), options.viewsdir || 'static/html');
    options.imgsdir = path.join(process.cwd(), options.imgsdir || 'static/imgs');
    options.fontsdir = path.join(process.cwd(), options.fontsdir || 'static/fonts');
    options.jsroute = options.jsroute || '/js';
    options.cssroute = options.cssroute || '/css';
    options.viewsroute = options.viewsroute || '/html';
    options.vendorroute = options.vendorroute || '/vendor';
    options.bowerroute = options.bowerroute || '/bower_components';
    options.fontroute = options.fontroute || '/fonts';
    options.imgsroute = options.imgsroute || '/imgs';
    ut.setupstatic(options.jsdir);
    ut.setupstatic(options.cssdir);
    if (options.app != null) {
      app = options.app;
      app.use(options.jsroute, harp.mount(options.jsdir));
      app.use(options.cssroute, harp.mount(options.cssdir));
      app.use(options.viewsroute, harp.mount(options.viewsdir));
      app.use(options.vendorroute, harp.mount('static/vendor'));
      app.use(options.bowerroute, harp.mount('static/bower_components'));
      app.use(options.fontroute, express["static"](options.fontsdir));
      app.use(options.imgsroute, express["static"](options.imgsdir));
      return app.use(express["static"](options.imgsdir));
    }
  };

  mountEntityParser = function(options) {
    var app, entityparser;
    if (options.app != null) {
      app = options.app;
      entityparser = options.entityparser || rp.entityparser;
      return app.use(rp.middlewarewrapper('entityname', entityparser));
    }
  };

  mountKeyValParser = function(options) {
    var app, urlparams;
    if (options.app != null) {
      app = options.app;
      urlparams = options.keyvalparser || rp.urlparamshelper;
      return app.use(rp.middlewarewrapper('urlparams', urlparams, options));
    }
  };

  mountDatabase = function(options) {
    var app, db;
    if (options.app != null) {
      app = options.app;
      if (options.database != null) {
        switch (options.database.backend) {
          case "couchbase":
            db = dbs.couchbase(options);
            break;
          case "lowdb":
            db = dbs.lowdb(options);
            break;
          case "nano":
            db = dbs.nano(options);
            break;
          default:
            db = dbs.couchbase(options);
        }
      } else {
        db = dbs.lowdb(options);
      }
      return app.use(function(req, res, next) {
        req.db = db;
        return next();
      });
    }
  };

  mountValidator = function(options) {
    var app;
    options.validator = options.validator || validators.apikey;
    if (options.app != null) {
      app = options.app;
      app.use(function(req, res, next) {
        req.skiproutes = options.render || [];
        return next();
      });
      return app.use(options.validator);
    }
  };

  mountLoginPage = function(options) {
    var app;
    if (options.app != null) {
      app = options.app;
      if (options.login != null) {
        return validators.loginpage(options);
      }
    }
  };

  mountSchemaLoader = function(options) {
    var app, schemadir;
    if (options.app != null) {
      app = options.app;
      schemadir = path.join(process.cwd(), options.schemadir || 'schemas');
      if (!fs.existsSync(schemadir)) {
        mkdirp(schemadir);
      }
      app.use(ut.setupschemaloader(schemadir));
      return app.use(ut.setupschemadir(schemadir));
    }
  };

  mountMiddleware = function(options) {
    var app, i, len, mdw, ref, results;
    if ((options.app != null) && (options.middleware != null)) {
      app = options.app;
      ref = options.middleware;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        mdw = ref[i];
        results.push(app.use(mdw));
      }
      return results;
    }
  };

  mountRoutes = function(options) {
    var handler, i, j, len, len1, ref, ref1, results;
    ref = options.routes;
    for (i = 0, len = ref.length; i < len; i++) {
      handler = ref[i];
      if (handler.isCustom()) {
        handler.attach(options.app, options.validator);
      }
    }
    ref1 = options.routes;
    results = [];
    for (j = 0, len1 = ref1.length; j < len1; j++) {
      handler = ref1[j];
      if (!handler.isCustom()) {
        results.push(handler.attach(options.app, options.validator));
      }
    }
    return results;
  };

  mountToolkit = function(options) {
    if (options.app != null) {
      return tk.attach(options.app);
    }
  };

  exports.mount = function(options) {
    var routes;
    routes = rp.parse(options);
    return mountStatic(options);
  };

  mountConnect = function(options) {
    var app;
    if (options.app != null) {
      app = options.app;
      app.use(bodyparser.json());
      app.use(bodyparser.urlencoded({
        extended: true
      }));
      return app.use(cookieparser(options.cookiesignature, options.cookieoptions));
    }
  };

  exports.init = function(options) {
    var app;
    options = options || {};
    app = options.app || setupapp(options);
    app.set('view engine', 'jade');
    options.routes = options.routes || rp.parse(options);
    mountConnect(options);
    mountDatabase(options);
    mountToolkit(options);
    mountEntityParser(options);
    mountKeyValParser(options);
    mountSchemaLoader(options);
    mountMiddleware(options);
    mountRoutes(options);
    mountStatic(options);
    return app;
  };

}).call(this);
