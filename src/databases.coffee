path = require 'path'
fs = require 'fs'
mkdirp = require 'mkdirp'
bb = require 'bluebird'

exports.couchbase = (options) ->
  cb = require 'couchbase'
  if options.database?
    conn =
      'bucket': options.database.bucket || 'grassroots'
      'host' : options.database.host || '127.0.0.1:8091'
  else
    conn =
      'bucket': 'grassroots'
      'host' : '127.0.0.1:8091'
  db = new cb.Connection conn ,
    (err) ->
      if err?
        throw err
  db

exports.lowdb = (options) ->
  db = require 'lowdb'
  pth = options.dbpath || 'data/db.json'
  db.path = path.join( process.cwd(), pth )
  if not fs.existsSync db.path
    fs.writeFileSync db.path, "{}"
  db.load()
  db

exports.nano = (options) ->
  db = require 'nano'
  cn = options.database.host || 'http://localhost:5984'
  bucket = options.database.bucket || 'grassroots'
  db = db cn
  db = db.use bucket
  bb.promisifyAll db
  db
