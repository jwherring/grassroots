passport = require 'passport'
startswith = require('./toolkit').startswith

checksignature = (key, data, signature) ->
  shasum = crypto.createHash 'sha256'
  shasum.update "#{key}#{data}"
  shasum.digest 'hex' is signature

exports.apikey = (req, res, next) ->
  authheader = req.get 'Authorization'
  if req.path in req.skiproutes or startswith(req.path, '/js') or startswith(req.path, '/css')
    next()
  else
    if authheader?
      [account, payload] = authheader.split '::'
      req.user = account
      if req.method in ['PUT', 'POST']
        console.log "(#{req.method} -> READ BODY) ACCOUNT: #{account}; PAYLOAD: #{payload}"
      else
        console.log "(#{req.method} -> READ URL) ACCOUNT: #{account}; PAYLOAD: #{payload}"
      next()
    else
      res.status(401).send('Authorization required')

exports.apikey
