ld = require 'lodash'
crypto = require 'crypto'

chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz'

checktype = (obj) ->
  if obj == undefined or obj == null
    return String obj
  classToType = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regexp',
    '[object Object]': 'object'
  }
  classToType[Object.prototype.toString.call(obj)]

serialize = (obj) ->
  switch checktype obj
    when 'object'
      keys = ld.keys obj
      keys.sort()
      ("#{k}#{serialize obj[k]}" for k in keys).join ''
    else
      "#{obj}"
  
tklibrary =
  apikeygen: (len) ->
    len = len ? 32
    (chars[Math.floor(Math.random() * chars.length)] for i in [0..len]).join ''

  checktype: checktype

  gensignature: (key, input) ->
    hmac = crypto.createHmac 'md5', key
    hmac.update serialize input
    hmac.digest 'hex'

  hashpassword: (password) ->
    salt = tklibrary.apikeygen 32
    digest = tklibrary.gensignature salt, password
    "#{salt}$:1:$#{digest}"

  checkpassword: (password, hashed) ->
    [salt, digest] = hashed.split '$:1:$'
    tklibrary.gensignature(salt, password) is digest

exports.attach = (app) ->
  app.use (req, res, next) ->
    req.toolkit = tklibrary
    next()

exports.startswith = (str, pat, pos) ->
  pos = pos || 0
  str.lastIndexOf(pat, pos) is pos
