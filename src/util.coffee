fs = require 'fs'
ld = require 'lodash'
tv4 = require 'tv4'
bb = require 'bluebird'
path = require 'path'
mkdirp = require 'mkdirp'
crypto = require 'crypto'
cs = require 'coffee-script/register'

dateTimeFormat = (data, schema) ->
  if typeof(data) is 'string' and /^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/.test data
    null
  else
    'date-time members must be in format YYYY-MM-DD HH:MM:SS'

tv4.addFormat 'date-time', dateTimeFormat

exports.setupstatic = (pathname) ->
  pathname = path.join process.cwd(), pathname
  if not fs.existsSync pathname
    mkdirp pathname

exports.setupschemaloader = (schemadir) ->
  (req, res, next) ->
    req.loadschema = (entityname) ->
      fl = path.join(schemadir, "#{entityname}.coffee")
      loadedschema = require(fl).schema
      loadedschema
    req.loaddefaults = (entityname) ->
      fl = path.join(schemadir, "#{entityname}.coffee")
      loadeddefaults = require(fl).defaults
      loadeddefaults
    next()

exports.setupschemadir = (schemadir) ->
  (req, res, next) ->
    if req.method in ['PUT', 'POST'] and req.entityname?
      fl = path.join(schemadir, "#{req.entityname}.coffee")
      if fs.existsSync(fl)
        req.entityschema = require(fl).schema
        req.defaultsschema = require(fl).defaults
        req.validateentity = (entity, entityschema, defaultsschema) ->
          entity ?= req.body
          entityschema = if entityschema? then req.loadschema entityschema else req.entityschema
          #console.log entityschema
          defaultsschema = if defaultsschema? then req.loaddefaults defaultsschema else req.defaultsschema
          if req.method is 'PUT' and entityschema?
            entityschema = ld.clone entityschema
            delete entityschema.required
          if req.method is 'POST' and defaultsschema?
            entity[k] = v for k,v of defaultsschema when not ld.has entity, k
          if tv4.validate entity, entityschema
            {"valid": true}
          else
            tv4.error
        req.validateentityAsync = (entity, entityschema, defaultsschema) ->
          new bb (resolve, reject) ->
            entity ?= req.body
            entityschema = if entityschema? then req.loadschema entityschema else req.entityschema
            #console.log entityschema
            defaultsschema = if defaultsschema? then req.loaddefaults defaultsschema else req.defaultsschema
            if req.method is 'PUT' and entityschema?
              entityschema = ld.clone entityschema
              delete entityschema.required
            if req.method is 'POST' and defaultsschema?
              entity[k] = v for k,v of defaultsschema when not (k of entity)
              console.log entity
            if tv4.validate entity, entityschema
              resolve {"valid": true}
            else
              reject tv4.error
    next()

