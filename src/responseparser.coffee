RouteHandler = require('./routehandler').RouteHandler

attach = (handler, app) ->
  console.log handler
  app[handler.method]( handler.path, handler.implementation )

exports.parse = (fh, app) ->
  module = require(fh)
  resource = fh.replace /\.coffee$/, ''
  components = resource.split '/'
  resource = components[components.length - 1 ]
  console.log "RESOURCE: #{resource}"
  funcs = (new RouteHandler {methodName: f, resourceName: resource, implementation: module[f]} for f of module)
