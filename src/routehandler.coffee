class RouteHandler
  constructor: (@options) ->
    @methodName = @options.methodName
    @resourceName = @options.resourceName
    @implementation = @options.implementation
    @app = @options.app
    @getMethod()
    @getAction()
    @logRoute()
    @makePath()

  getMethod: () ->
    @method = /^[a-z]*/.exec(@methodName)[0]

  getAction: () ->
    @action = @methodName.replace @method, ''

  logRoute: () ->
    @route = "#{@method.toUpperCase()} #{@methodName}"

  makePath: () ->
    if @method is 'get' and @action and @action is 'Index'
      @prefix = "^\/#{@resourceName}"
      @path = new RegExp "#{@prefix}(\/(.*))?"
    else if @method is 'post' and not @action
      @prefix = "^\/#{@resourceName}"
      @path = new RegExp "#{@prefix}"
    else if @action and @action isnt 'Index'
      @prefix = "^\/#{@resourceName}\/#{@action.toLowerCase()}"
      @path = new RegExp "#{@prefix}(\/(.*))?"
    else
      @prefix = "^\/#{@resourceName}\/id"
      @path = new RegExp "#{@prefix}\/(.+)(\/(.*))?"

  attach: (app, validator) ->
    console.log "ATTACHING: #{@path}::#{@method}"
    if validator?
      app[@method](@path, @implementation)
      #app[@method](@path, validator, @implementation)
    else
      app[@method](@path, @implementation)

  isCustom: () ->
    @action isnt '' and @action isnt 'Index'


class NestedRouteHandler extends RouteHandler
  constructor: (@options) ->
    @parent = @options.parentResource
    super(@options)


exports.RouteHandler = RouteHandler
exports.NestedRouteHandler = NestedRouteHandler
