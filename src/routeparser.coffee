fs = require 'fs'
cs = require 'coffee-script/register'
rp = require './responseparser'
s = require 'string'
ld = require 'lodash'
path = require 'path'

get_directories = (dd) ->
  (fd for fd in fs.readdirSync dd when fs.lstatSync("#{dd}/#{fd}").isDirectory())

get_full = (dd) ->
  (path.resolve fd for fd in fs.readdirSync dd)

get_scripts = (dd) ->
  (fd for fd in fs.readdirSync dd when /\.coffee$/.test fd)

compare_path = (a,b) ->
  console.log "SORT: #{a}, #{b}"
  console.log a
  if a.length < b.length and s(b).startsWith a
    console.log "SORT: #{a}, #{b}"
    1
  else
    0

exports.parse = (options) ->
  options = options || {}
  app = options.app
  routesdir = options.routesdir || process.cwd() + '/routes'
  routelist = get_scripts routesdir
  for k,i in routelist
    if i < routelist.length - 1
      n = routelist[i+1]
      console.log "TEST #{k}, #{n}"
      if n.length > k.length and s(n).startsWith s(k).chompRight('.coffee').toString()
        routelist[i] = n
        routelist[i+1] = k
  others = get_directories routesdir
  handlers = ld.flatten (rp.parse("#{routesdir}/#{rt}", app) for rt in routelist)
  handlers

pathIsCustom = (pth, customroutes) ->
  ld.some((pth.lastIndexOf(rt) is 0 for rt in customroutes))

exports.urlparamshelper = (pth, options) ->
  customroutes = (rt.prefix for rt in options.routes when rt.isCustom())
  pathiscustom = pathIsCustom pth, customroutes
  #console.log "CUSTOM: #{pathiscustom}"
  urlparams = pth.replace /^\//, ''
  urlparams = urlparams.replace /\/$/, ''
  urlparams = urlparams.split '/'
  catcher = urlparams.shift()
  if pathiscustom
    urlparams.shift()
  #if urlparams[0] is 'id' or pathiscustom
  #  urlparams.shift()
  #  urlparams.shift()
  if urlparams.length % 2 isnt 0
    urlparams.pop()
  urlkeys = (decodeURIComponent urlparams[i] for i in [0..urlparams.length - 2] by 2)
  urlvals = (decodeURIComponent urlparams[i] for i in [1..urlparams.length - 1] by 2)
  urlparams = ld.zipObject urlkeys, urlvals
  urlparams

exports.entityparser = (pth) ->
  urlparams = pth.replace /^\//, ''
  urlparams = urlparams.replace /\/$/, ''
  urlparams = urlparams.split '/'
  urlparams.shift()

exports.middlewarewrapper = (varname, helper, options) ->
  (req, res, next) ->
    req[varname] = helper(req.path, options)
    next()
