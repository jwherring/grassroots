gulp = require 'gulp'
gutil = require 'gulp-util'
coffee = require 'gulp-coffee'

gulp.task 'coffee', () ->
  gulp.src('grassroots.coffee')
    .pipe coffee()
    .pipe gulp.dest './'
  gulp.src('./src/*.coffee')
    .pipe coffee()
    .pipe gulp.dest './lib/'
