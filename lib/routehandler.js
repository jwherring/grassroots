(function() {
  var NestedRouteHandler, RouteHandler,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  RouteHandler = (function() {
    function RouteHandler(options) {
      this.options = options;
      this.methodName = this.options.methodName;
      this.resourceName = this.options.resourceName;
      this.implementation = this.options.implementation;
      this.app = this.options.app;
      this.getMethod();
      this.getAction();
      this.logRoute();
      this.makePath();
    }

    RouteHandler.prototype.getMethod = function() {
      return this.method = /^[a-z]*/.exec(this.methodName)[0];
    };

    RouteHandler.prototype.getAction = function() {
      return this.action = this.methodName.replace(this.method, '');
    };

    RouteHandler.prototype.logRoute = function() {
      return this.route = (this.method.toUpperCase()) + " " + this.methodName;
    };

    RouteHandler.prototype.makePath = function() {
      if (this.method === 'get' && this.action && this.action === 'Index') {
        this.prefix = "^\/" + this.resourceName;
        return this.path = new RegExp(this.prefix + "(\/(.*))?");
      } else if (this.method === 'post' && !this.action) {
        this.prefix = "^\/" + this.resourceName;
        return this.path = new RegExp("" + this.prefix);
      } else if (this.action && this.action !== 'Index') {
        this.prefix = "^\/" + this.resourceName + "\/" + (this.action.toLowerCase());
        return this.path = new RegExp(this.prefix + "(\/(.*))?");
      } else {
        this.prefix = "^\/" + this.resourceName + "\/id";
        return this.path = new RegExp(this.prefix + "\/(.+)(\/(.*))?");
      }
    };

    RouteHandler.prototype.attach = function(app, validator) {
      console.log("ATTACHING: " + this.path + "::" + this.method);
      if (validator != null) {
        return app[this.method](this.path, this.implementation);
      } else {
        return app[this.method](this.path, this.implementation);
      }
    };

    RouteHandler.prototype.isCustom = function() {
      return this.action !== '' && this.action !== 'Index';
    };

    return RouteHandler;

  })();

  NestedRouteHandler = (function(superClass) {
    extend(NestedRouteHandler, superClass);

    function NestedRouteHandler(options) {
      this.options = options;
      this.parent = this.options.parentResource;
      NestedRouteHandler.__super__.constructor.call(this, this.options);
    }

    return NestedRouteHandler;

  })(RouteHandler);

  exports.RouteHandler = RouteHandler;

  exports.NestedRouteHandler = NestedRouteHandler;

}).call(this);
