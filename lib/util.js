(function() {
  var bb, crypto, cs, dateTimeFormat, fs, ld, mkdirp, path, tv4;

  fs = require('fs');

  ld = require('lodash');

  tv4 = require('tv4');

  bb = require('bluebird');

  path = require('path');

  mkdirp = require('mkdirp');

  crypto = require('crypto');

  cs = require('coffee-script/register');

  dateTimeFormat = function(data, schema) {
    if (typeof data === 'string' && /^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/.test(data)) {
      return null;
    } else {
      return 'date-time members must be in format YYYY-MM-DD HH:MM:SS';
    }
  };

  tv4.addFormat('date-time', dateTimeFormat);

  exports.setupstatic = function(pathname) {
    pathname = path.join(process.cwd(), pathname);
    if (!fs.existsSync(pathname)) {
      return mkdirp(pathname);
    }
  };

  exports.setupschemaloader = function(schemadir) {
    return function(req, res, next) {
      req.loadschema = function(entityname) {
        var fl, loadedschema;
        fl = path.join(schemadir, entityname + ".coffee");
        loadedschema = require(fl).schema;
        return loadedschema;
      };
      req.loaddefaults = function(entityname) {
        var fl, loadeddefaults;
        fl = path.join(schemadir, entityname + ".coffee");
        loadeddefaults = require(fl).defaults;
        return loadeddefaults;
      };
      return next();
    };
  };

  exports.setupschemadir = function(schemadir) {
    return function(req, res, next) {
      var fl, ref;
      if (((ref = req.method) === 'PUT' || ref === 'POST') && (req.entityname != null)) {
        fl = path.join(schemadir, req.entityname + ".coffee");
        if (fs.existsSync(fl)) {
          req.entityschema = require(fl).schema;
          req.defaultsschema = require(fl).defaults;
          req.validateentity = function(entity, entityschema, defaultsschema) {
            var k, v;
            if (entity == null) {
              entity = req.body;
            }
            entityschema = entityschema != null ? req.loadschema(entityschema) : req.entityschema;
            defaultsschema = defaultsschema != null ? req.loaddefaults(defaultsschema) : req.defaultsschema;
            if (req.method === 'PUT' && (entityschema != null)) {
              entityschema = ld.clone(entityschema);
              delete entityschema.required;
            }
            if (req.method === 'POST' && (defaultsschema != null)) {
              for (k in defaultsschema) {
                v = defaultsschema[k];
                if (!ld.has(entity, k)) {
                  entity[k] = v;
                }
              }
            }
            if (tv4.validate(entity, entityschema)) {
              return {
                "valid": true
              };
            } else {
              return tv4.error;
            }
          };
          req.validateentityAsync = function(entity, entityschema, defaultsschema) {
            return new bb(function(resolve, reject) {
              var k, v;
              if (entity == null) {
                entity = req.body;
              }
              entityschema = entityschema != null ? req.loadschema(entityschema) : req.entityschema;
              defaultsschema = defaultsschema != null ? req.loaddefaults(defaultsschema) : req.defaultsschema;
              if (req.method === 'PUT' && (entityschema != null)) {
                entityschema = ld.clone(entityschema);
                delete entityschema.required;
              }
              if (req.method === 'POST' && (defaultsschema != null)) {
                for (k in defaultsschema) {
                  v = defaultsschema[k];
                  if (!(k in entity)) {
                    entity[k] = v;
                  }
                }
                console.log(entity);
              }
              if (tv4.validate(entity, entityschema)) {
                return resolve({
                  "valid": true
                });
              } else {
                return reject(tv4.error);
              }
            });
          };
        }
      }
      return next();
    };
  };

}).call(this);
