(function() {
  var chars, checktype, crypto, ld, serialize, tklibrary;

  ld = require('lodash');

  crypto = require('crypto');

  chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz';

  checktype = function(obj) {
    var classToType;
    if (obj === void 0 || obj === null) {
      return String(obj);
    }
    classToType = {
      '[object Boolean]': 'boolean',
      '[object Number]': 'number',
      '[object String]': 'string',
      '[object Function]': 'function',
      '[object Array]': 'array',
      '[object Date]': 'date',
      '[object RegExp]': 'regexp',
      '[object Object]': 'object'
    };
    return classToType[Object.prototype.toString.call(obj)];
  };

  serialize = function(obj) {
    var k, keys;
    switch (checktype(obj)) {
      case 'object':
        keys = ld.keys(obj);
        keys.sort();
        return ((function() {
          var j, len1, results;
          results = [];
          for (j = 0, len1 = keys.length; j < len1; j++) {
            k = keys[j];
            results.push("" + k + (serialize(obj[k])));
          }
          return results;
        })()).join('');
      default:
        return "" + obj;
    }
  };

  tklibrary = {
    apikeygen: function(len) {
      var i;
      len = len != null ? len : 32;
      return ((function() {
        var j, ref, results;
        results = [];
        for (i = j = 0, ref = len; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
          results.push(chars[Math.floor(Math.random() * chars.length)]);
        }
        return results;
      })()).join('');
    },
    checktype: checktype,
    gensignature: function(key, input) {
      var hmac;
      hmac = crypto.createHmac('md5', key);
      hmac.update(serialize(input));
      return hmac.digest('hex');
    },
    hashpassword: function(password) {
      var digest, salt;
      salt = tklibrary.apikeygen(32);
      digest = tklibrary.gensignature(salt, password);
      return salt + "$:1:$" + digest;
    },
    checkpassword: function(password, hashed) {
      var digest, ref, salt;
      ref = hashed.split('$:1:$'), salt = ref[0], digest = ref[1];
      return tklibrary.gensignature(salt, password) === digest;
    }
  };

  exports.attach = function(app) {
    return app.use(function(req, res, next) {
      req.toolkit = tklibrary;
      return next();
    });
  };

  exports.startswith = function(str, pat, pos) {
    pos = pos || 0;
    return str.lastIndexOf(pat, pos) === pos;
  };

}).call(this);
