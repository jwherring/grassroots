(function() {
  var checksignature, passport, startswith,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  passport = require('passport');

  startswith = require('./toolkit').startswith;

  checksignature = function(key, data, signature) {
    var shasum;
    shasum = crypto.createHash('sha256');
    shasum.update("" + key + data);
    return shasum.digest('hex' === signature);
  };

  exports.apikey = function(req, res, next) {
    var account, authheader, payload, ref, ref1, ref2;
    authheader = req.get('Authorization');
    if ((ref = req.path, indexOf.call(req.skiproutes, ref) >= 0) || startswith(req.path, '/js') || startswith(req.path, '/css')) {
      return next();
    } else {
      if (authheader != null) {
        ref1 = authheader.split('::'), account = ref1[0], payload = ref1[1];
        req.user = account;
        if ((ref2 = req.method) === 'PUT' || ref2 === 'POST') {
          console.log("(" + req.method + " -> READ BODY) ACCOUNT: " + account + "; PAYLOAD: " + payload);
        } else {
          console.log("(" + req.method + " -> READ URL) ACCOUNT: " + account + "; PAYLOAD: " + payload);
        }
        return next();
      } else {
        return res.status(401).send('Authorization required');
      }
    }
  };

  exports.apikey;

}).call(this);
