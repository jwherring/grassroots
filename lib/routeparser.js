(function() {
  var compare_path, cs, fs, get_directories, get_full, get_scripts, ld, path, pathIsCustom, rp, s;

  fs = require('fs');

  cs = require('coffee-script/register');

  rp = require('./responseparser');

  s = require('string');

  ld = require('lodash');

  path = require('path');

  get_directories = function(dd) {
    var fd, j, len, ref, results;
    ref = fs.readdirSync(dd);
    results = [];
    for (j = 0, len = ref.length; j < len; j++) {
      fd = ref[j];
      if (fs.lstatSync(dd + "/" + fd).isDirectory()) {
        results.push(fd);
      }
    }
    return results;
  };

  get_full = function(dd) {
    var fd, j, len, ref, results;
    ref = fs.readdirSync(dd);
    results = [];
    for (j = 0, len = ref.length; j < len; j++) {
      fd = ref[j];
      results.push(path.resolve(fd));
    }
    return results;
  };

  get_scripts = function(dd) {
    var fd, j, len, ref, results;
    ref = fs.readdirSync(dd);
    results = [];
    for (j = 0, len = ref.length; j < len; j++) {
      fd = ref[j];
      if (/\.coffee$/.test(fd)) {
        results.push(fd);
      }
    }
    return results;
  };

  compare_path = function(a, b) {
    console.log("SORT: " + a + ", " + b);
    console.log(a);
    if (a.length < b.length && s(b).startsWith(a)) {
      console.log("SORT: " + a + ", " + b);
      return 1;
    } else {
      return 0;
    }
  };

  exports.parse = function(options) {
    var app, handlers, i, j, k, len, n, others, routelist, routesdir, rt;
    options = options || {};
    app = options.app;
    routesdir = options.routesdir || process.cwd() + '/routes';
    routelist = get_scripts(routesdir);
    for (i = j = 0, len = routelist.length; j < len; i = ++j) {
      k = routelist[i];
      if (i < routelist.length - 1) {
        n = routelist[i + 1];
        console.log("TEST " + k + ", " + n);
        if (n.length > k.length && s(n).startsWith(s(k).chompRight('.coffee').toString())) {
          routelist[i] = n;
          routelist[i + 1] = k;
        }
      }
    }
    others = get_directories(routesdir);
    handlers = ld.flatten((function() {
      var l, len1, results;
      results = [];
      for (l = 0, len1 = routelist.length; l < len1; l++) {
        rt = routelist[l];
        results.push(rp.parse(routesdir + "/" + rt, app));
      }
      return results;
    })());
    return handlers;
  };

  pathIsCustom = function(pth, customroutes) {
    var rt;
    return ld.some((function() {
      var j, len, results;
      results = [];
      for (j = 0, len = customroutes.length; j < len; j++) {
        rt = customroutes[j];
        results.push(pth.lastIndexOf(rt) === 0);
      }
      return results;
    })());
  };

  exports.urlparamshelper = function(pth, options) {
    var catcher, customroutes, i, pathiscustom, rt, urlkeys, urlparams, urlvals;
    customroutes = (function() {
      var j, len, ref, results;
      ref = options.routes;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        rt = ref[j];
        if (rt.isCustom()) {
          results.push(rt.prefix);
        }
      }
      return results;
    })();
    pathiscustom = pathIsCustom(pth, customroutes);
    urlparams = pth.replace(/^\//, '');
    urlparams = urlparams.replace(/\/$/, '');
    urlparams = urlparams.split('/');
    catcher = urlparams.shift();
    if (pathiscustom) {
      urlparams.shift();
    }
    if (urlparams.length % 2 !== 0) {
      urlparams.pop();
    }
    urlkeys = (function() {
      var j, ref, results;
      results = [];
      for (i = j = 0, ref = urlparams.length - 2; j <= ref; i = j += 2) {
        results.push(decodeURIComponent(urlparams[i]));
      }
      return results;
    })();
    urlvals = (function() {
      var j, ref, results;
      results = [];
      for (i = j = 1, ref = urlparams.length - 1; j <= ref; i = j += 2) {
        results.push(decodeURIComponent(urlparams[i]));
      }
      return results;
    })();
    urlparams = ld.zipObject(urlkeys, urlvals);
    return urlparams;
  };

  exports.entityparser = function(pth) {
    var urlparams;
    urlparams = pth.replace(/^\//, '');
    urlparams = urlparams.replace(/\/$/, '');
    urlparams = urlparams.split('/');
    return urlparams.shift();
  };

  exports.middlewarewrapper = function(varname, helper, options) {
    return function(req, res, next) {
      req[varname] = helper(req.path, options);
      return next();
    };
  };

}).call(this);
