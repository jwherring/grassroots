(function() {
  var bb, fs, mkdirp, path;

  path = require('path');

  fs = require('fs');

  mkdirp = require('mkdirp');

  bb = require('bluebird');

  exports.couchbase = function(options) {
    var cb, conn, db;
    cb = require('couchbase');
    if (options.database != null) {
      conn = {
        'bucket': options.database.bucket || 'grassroots',
        'host': options.database.host || '127.0.0.1:8091'
      };
    } else {
      conn = {
        'bucket': 'grassroots',
        'host': '127.0.0.1:8091'
      };
    }
    db = new cb.Connection(conn, function(err) {
      if (err != null) {
        throw err;
      }
    });
    return db;
  };

  exports.lowdb = function(options) {
    var db, pth;
    db = require('lowdb');
    pth = options.dbpath || 'data/db.json';
    db.path = path.join(process.cwd(), pth);
    if (!fs.existsSync(db.path)) {
      fs.writeFileSync(db.path, "{}");
    }
    db.load();
    return db;
  };

  exports.nano = function(options) {
    var bucket, cn, db;
    db = require('nano');
    cn = options.database.host || 'http://localhost:5984';
    bucket = options.database.bucket || 'grassroots';
    db = db(cn);
    db = db.use(bucket);
    bb.promisifyAll(db);
    return db;
  };

}).call(this);
