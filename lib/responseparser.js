(function() {
  var RouteHandler, attach;

  RouteHandler = require('./routehandler').RouteHandler;

  attach = function(handler, app) {
    console.log(handler);
    return app[handler.method](handler.path, handler.implementation);
  };

  exports.parse = function(fh, app) {
    var components, f, funcs, module, resource;
    module = require(fh);
    resource = fh.replace(/\.coffee$/, '');
    components = resource.split('/');
    resource = components[components.length - 1];
    console.log("RESOURCE: " + resource);
    return funcs = (function() {
      var results;
      results = [];
      for (f in module) {
        results.push(new RouteHandler({
          methodName: f,
          resourceName: resource,
          implementation: module[f]
        }));
      }
      return results;
    })();
  };

}).call(this);
