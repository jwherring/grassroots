path = require 'path'
fs = require 'fs'
harp = require 'harp'
express = require 'express'
bodyparser = require 'body-parser'
cookieparser = require 'cookie-parser'
mkdirp = require 'mkdirp'
tv = require 'tv4'
ld = require 'lodash'
rp = require './lib/routeparser'
ut = require './lib/util'
tk = require './lib/toolkit'
dbs = require './lib/databases'
validators = require './lib/validators'

#make sure there is an app, initializing an express 
#app if one hasn't been passed in by the caller
setupapp = (options) ->
  app = options.app || express()
  if not options.app?
    options.app = app
  app

#mount the harp js and css directories.  These deftault to static/js
#and static/css.  Allow caller to override.
mountStatic = (options) ->
  options.jsdir = path.join( process.cwd(), options.jsdir || 'static/js')
  options.cssdir = path.join( process.cwd(), options.cssdir || 'static/css')
  options.viewsdir = path.join( process.cwd(), options.viewsdir || 'static/html')
  options.imgsdir = path.join( process.cwd(), options.imgsdir || 'static/imgs')
  options.fontsdir = path.join( process.cwd(), options.fontsdir || 'static/fonts')
  options.jsroute = options.jsroute || '/js'
  options.cssroute = options.cssroute || '/css'
  options.viewsroute = options.viewsroute || '/html'
  options.vendorroute = options.vendorroute || '/vendor'
  options.bowerroute = options.bowerroute || '/bower_components'
  options.fontroute = options.fontroute || '/fonts'
  options.imgsroute = options.imgsroute || '/imgs'
  ut.setupstatic options.jsdir
  ut.setupstatic options.cssdir
  if options.app?
    app = options.app
    app.use options.jsroute , harp.mount options.jsdir
    app.use options.cssroute , harp.mount options.cssdir
    app.use options.viewsroute , harp.mount options.viewsdir
    app.use options.vendorroute , harp.mount 'static/vendor'
    app.use options.bowerroute , harp.mount 'static/bower_components'
    app.use options.fontroute, express.static options.fontsdir
    app.use options.imgsroute, express.static options.imgsdir
    app.use express.static options.imgsdir

#Set up the logic by which grassroots knows what counts as an entity.
#Allow the caller to override the default behavior.  The default
#is to treat any file in /routes as an entity manager and attach all
#methods of the exported object as endpoints
mountEntityParser = (options) ->
  if options.app?
    app = options.app
    entityparser = options.entityparser || rp.entityparser
    app.use rp.middlewarewrapper 'entityname', entityparser
    
#Set up key/value parsing for incoming requests.  By default, 
#this will take everything after the action 
#name as a key-val pair.  I.e. /action/id/idval/one/oneval/two/twoval  
#will result in the following dictionary accessible from req.urlparams:
#
#  {
#   "id": "idval",
#   "one": "oneval",
#   "two": "twoval"
#  }
mountKeyValParser = (options) ->
  if options.app?
    app = options.app
    urlparams = options.keyvalparser || rp.urlparamshelper
    app.use rp.middlewarewrapper 'urlparams', urlparams, options


#Mount a generic couchbase database if none is specified
mountDatabase = (options) ->
  if options.app?
    app = options.app
    if options.database?
      switch options.database.backend
        when "couchbase"
          db = dbs.couchbase options
        when "lowdb"
          db = dbs.lowdb options
        when "nano"
          db = dbs.nano options
        else
          db = dbs.couchbase options
    else
      db = dbs.lowdb options
    app.use (req, res, next) ->
      req.db = db
      next()

#INCOMPLETE - validate request body (POST, PUT) or 
#request url (GET, DELETE) against a hashed value
#provided in the "Authenticate" header
#mountValidator = (options) ->
#  if options.app?
#    app = options.app
#    if options.validator? and ld.isFunction options.validator
#      app.use options.validator
#    else if options.validator? and ld.isString options.validator
#      switch options.validator
#        when "apikey"
#          app.use validators.apikey
#    else if options.app?
#      app.use validators.apikey
mountValidator = (options) ->
  options.validator = options.validator || validators.apikey
  if options.app?
    app = options.app
    app.use (req, res, next) ->
      req.skiproutes = options.render || []
      next()
    app.use options.validator

#INCOMPLETE/NOT CALLED - default login page mounted at "/login"
mountLoginPage = (options) ->
  if options.app?
    app = options.app
    if options.login?
      validators.loginpage options

#INCOMPLETE - optional middleware for loading tv4 schemas 
#to validate incoming requests against
mountSchemaLoader = (options) ->
  if options.app?
    app = options.app
    schemadir = path.join( process.cwd(), options.schemadir || 'schemas')
    if not fs.existsSync schemadir
      mkdirp schemadir
    app.use ut.setupschemaloader schemadir
    app.use ut.setupschemadir schemadir

#Cycle through a list of provided Connect middleware functions
#and attach to app.  This is for middleware that needs to be supplied 
#before the RESTful entity routes are mounted
mountMiddleware = (options) ->
  if options.app? and options.middleware?
    app = options.app
    app.use mdw for mdw in options.middleware

#Attachment function for routes.  Takes a list of RouteHandler classes and 
#mounts them as middleware.
mountRoutes = (options) ->
  handler.attach options.app, options.validator for handler in options.routes when handler.isCustom()
  handler.attach options.app, options.validator for handler in options.routes when not handler.isCustom()

mountToolkit = (options) ->
  if options.app?
    tk.attach options.app

#Convenience function to be used instead of init() when user 
#merely wants the RESTful routes attached
exports.mount = (options) ->
  routes = rp.parse options
  mountStatic options

mountConnect = (options) ->
  if options.app?
    app = options.app
    app.use bodyparser.json()
    app.use bodyparser.urlencoded({extended: true})
    app.use cookieparser options.cookiesignature, options.cookieoptions

#Entry point.  Applies all helper functions specified in this file.
exports.init = (options) ->
  options = options || {}
  app = options.app || setupapp options
  app.set 'view engine', 'jade'
  options.routes = options.routes || rp.parse options
  mountConnect options
  mountDatabase options
  mountToolkit options
  #mountValidator options
  mountEntityParser options
  mountKeyValParser options
  mountSchemaLoader options
  mountMiddleware options
  mountRoutes options
  mountStatic options
  app
